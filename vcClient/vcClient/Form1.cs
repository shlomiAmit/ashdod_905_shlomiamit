﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.IO;
using System.Net;

namespace vcClient
{
    public partial class Form1 : Form
    {
        
        public NetworkStream clientStream;
        public Form1()
        {
            InitializeComponent();
        }

        private void splitter1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                TcpClient client = new TcpClient();
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("192.168.1.146"), 8820);
                client.Connect(serverEndPoint);
                clientStream = client.GetStream();
                //byte[] buffer = new ASCIIEncoding().GetBytes("Hello Server!");
                //clientStream.Write(buffer, 0, buffer.Length);
                //clientStream.Flush();
                //buffer = new byte[4096];
                //int bytesRead = clientStream.Read(buffer, 0, 4096);
            }
            catch (Exception ep) { }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Form3 f = new Form3(this);
            f.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            byte[] buffer = new byte[4096];
            string str = "200";
            string temp = txtUser.Text.Length.ToString();
            temp = temp.PadLeft(2, '0');
            str += temp+txtUser.Text;
            temp = txtPass.Text.Length.ToString();
            temp = temp.PadLeft(2, '0');
            str += temp + txtPass.Text;
            clientStream.Write(new ASCIIEncoding().GetBytes(str),0,str.Length);
            int bytes = clientStream.Read(buffer, 0, buffer.Length);
            string result = System.Text.Encoding.UTF8.GetString(buffer);
            result.Trim();
            switch (result[3])
                {
                case '0':
                    {
                        Form2 fr = new Form2(this);
                        this.Hide();
                        fr.Show();
                        break;
                    }
                case '1':
                    {
                        MessageBox.Show("Wrong Details! ");
                        break;
                    }
                case '2':
                    {
                        MessageBox.Show("User is alredy conected! ");
                        break;
                    }
            }
               
        }
    }
}
