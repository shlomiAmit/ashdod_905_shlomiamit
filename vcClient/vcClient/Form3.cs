﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace vcClient
{
    public partial class Form3 : Form
    {
        private Form1 frm;
        public Form3(Form1 frm)
        {
            InitializeComponent();
            this.frm = frm;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            byte[] buffer = new byte[4096];
            string str = "203";
            string temp = txtUserSu.Text.Length.ToString();
            temp = temp.PadLeft(2, '0');
            str += temp + txtUserSu.Text;
            temp = txtPassSu.Text.Length.ToString();
            temp = temp.PadLeft(2, '0');
            str += temp + txtPassSu.Text;
            temp = txtEmailSu.Text.Length.ToString();
            temp = temp.PadLeft(2, '0');
            str += temp + txtEmailSu.Text;
            frm.clientStream.Write(new ASCIIEncoding().GetBytes(str), 0, str.Length);
            int bytes = frm.clientStream.Read(buffer, 0, buffer.Length);
            string result = System.Text.Encoding.UTF8.GetString(buffer);
            result.Trim();
            switch (result[3])
            {
                case '0':
                    {
                        this.Dispose();
                        break;
                    }
                case '1':
                    {
                        MessageBox.Show("Password illegal! ");
                        break;
                    }
                case '2':
                    {
                        MessageBox.Show("User is alredy exist! ");
                        break;
                    }
                case '3':
                    {
                        MessageBox.Show("Username illegal! ");
                        break;
                    }
                case '4':
                    {
                        MessageBox.Show("Someother problem.. ");
                        break;
                    }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }
    }
}
